//go:build unit

package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetWarning(t *testing.T) {
	// arrange
	entriesByComponent = map[string][]Entry{
		"BATT": {
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.8,
				Component:    battery,
			},
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 1, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.7,
				Component:    battery,
			},
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 2, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.9,
				Component:    battery,
			},
		},
	}

	// act
	warning := battery.getWarning(RedLow)

	// assert
	assert.NotNil(t, warning)
}

func TestGetWarningReturnsNilWhenNotEnoughEntries(t *testing.T) {
	// arrange
	entriesByComponent = map[string][]Entry{
		"BATT": {
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.8,
				Component:    battery,
			},
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 1, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.7,
				Component:    battery,
			},
		},
	}

	// act
	warning := battery.getWarning(RedLow)

	// assert
	assert.Nil(t, warning)
}

func TestGetWarningReturnsNilWhenNotWithinTimePeriod(t *testing.T) {
	// arrange
	entriesByComponent = map[string][]Entry{
		"TSTAT": {
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          17.8,
				Component:    thermostat,
			},
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 1, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          17.7,
				Component:    thermostat,
			},
			{
				Timestamp:    time.Date(2018, time.January, 1, 0, 7, 0, 0, time.UTC),
				RedHighLimit: 17.0,
				RedLowLimit:  8.0,
				Val:          7.7,
				Component:    thermostat,
			},
		},
	}

	// act
	warning := thermostat.getWarning(RedLow)

	// assert
	assert.Nil(t, warning)
}

//go:build unit

package main

import (
	"io"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLog(t *testing.T) {
	// arrange
	warning := Warning{
		SatelliteID: 1000,
		Severity:    RedLow,
		Component:   "BATT",
		Timestamp:   time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC),
	}

	rescueStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	// act
	LogWarnings([]Warning{warning})

	// cleanup
	w.Close()
	out, _ := io.ReadAll(r)
	os.Stdout = rescueStdout

	// assert
	expected := `[
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T00:00:00Z"
    }
]
`
	assert.Equal(t, expected, string(out))
}

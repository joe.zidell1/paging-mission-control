//go:build functional

package main

import (
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFunctionalTest(t *testing.T) {
	// arrange
	rescueStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	// act
	readLines("input.txt")

	// cleanup
	w.Close()
	out, _ := io.ReadAll(r)
	os.Stdout = rescueStdout

	// assert
	expected := `[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
`
	assert.Equal(t, expected, string(out))
}

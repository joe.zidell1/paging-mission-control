//go:build unit

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// write tests for entry.go

func TestNewEntry(t *testing.T) {
	// arrange
	input := "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT"

	// act
	entry := NewEntry(input)

	// assert
	assert.Equal(t, entry.Timestamp.String(), "2018-01-01 23:02:11.302 +0000 UTC")
	assert.Equal(t, entry.SatelliteID, int32(1000))
	assert.Equal(t, entry.RedHighLimit, 17.0)
	assert.Equal(t, entry.RedLowLimit, 8.0)
	assert.Equal(t, entry.YellowHighLimit, 15.0)
	assert.Equal(t, entry.YellowLowLimit, 9.0)
	assert.Equal(t, entry.Val, 7.7)
	assert.Equal(t, entry.Component.Name, "BATT")
}

func TestHasPassedThreshold(t *testing.T) {
	// arrange
	entry := Entry{
		RedHighLimit: 17.0,
		RedLowLimit:  8.0,
		Val:          7.7,
		Component:    battery,
	}

	// act
	hasPassedThreshold, severity := entry.hasPassedThreshold()

	// assert
	assert.True(t, hasPassedThreshold)
	assert.Equal(t, severity, Severity("RED LOW"))
}

func TestHasNotPassedThreshold(t *testing.T) {
	// arrange
	entry := Entry{
		RedHighLimit: 17.0,
		RedLowLimit:  8.0,
		Val:          9.7,
		Component:    battery,
	}

	// act
	hasPassedThreshold, _ := entry.hasPassedThreshold()

	// assert
	assert.False(t, hasPassedThreshold)
}

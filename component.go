// This file holds information about a single component as well as the components themselves.
// It also has the logic for thresholds and checking all entries belonging to a component.
package main

import (
	"time"
)

// Component represents a component of the satellite.
type Component struct {
	Name      string
	Threshold Threshold
}

// Threshold represents the conditions that must be met for a warning to be issued.
type Threshold struct {
	Period     time.Duration
	Count      int
	LimitField string
}

// battery is the battery component of the satellite for which we track the voltage levels.
var battery = Component{
	Name: "BATT",
	Threshold: Threshold{
		Period:     5 * time.Minute,
		Count:      3,
		LimitField: "RedLowLimit",
	},
}

// thermostat is the thermostat component of the satellite from which we get temperature readings.
var thermostat = Component{
	Name: "TSTAT",
	Threshold: Threshold{
		Period:     5 * time.Minute,
		Count:      3,
		LimitField: "RedHighLimit",
	},
}

// components is a map of component names to their objects.
// At this time, we are checking battery voltage and thermostat temperature.
// There is a reasonable possibility that any of this may change in the future.
// This is being designed to be extended.
var components = map[string]Component{
	battery.Name:    battery,
	thermostat.Name: thermostat,
}

// entriesByComponent is a map of component names to their entries.
var entriesByComponent = map[string][]Entry{}

// getWarning checks if a component's entries have passed the threshold and returns a warning if so.
func (c Component) getWarning(severity Severity) *Warning {
	threshold := c.Threshold

	// return early if there aren't enough entries to check
	if len(entriesByComponent[c.Name]) < threshold.Count {
		return nil
	}

	// get the first and last entries
	firstEntry := entriesByComponent[c.Name][0]
	lastEntry := entriesByComponent[c.Name][len(entriesByComponent[c.Name])-1]

	// check if the time delta between the last and first entries is greater than the threshold period
	if lastEntry.Timestamp.Sub(firstEntry.Timestamp) > threshold.Period {
		return nil
	}

	// since we're logging this entry, clear all of this component's entries
	// so it isn't logged twice, and we restart the check
	entriesByComponent[c.Name] = nil

	// create a warning object
	warning := Warning{
		SatelliteID: firstEntry.SatelliteID,
		Severity:    severity,
		Component:   firstEntry.Component.Name,
		Timestamp:   firstEntry.Timestamp,
	}
	return &warning
}

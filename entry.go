// This file holds information about a single entry as we as Severity levels.
package main

import (
	"strconv"
	"strings"
	"time"
)

// Entry represents a single line of input.
// All number fields aside from SatelliteID are floats since they may contain decimals.
type Entry struct {
	Timestamp       time.Time
	SatelliteID     int32
	RedHighLimit    float64
	RedLowLimit     float64
	YellowHighLimit float64
	YellowLowLimit  float64
	Val             float64
	Component       Component
}

// NewEntry creates a new Entry from a line of input.
// The line of input is expected to be in the format:
//
//	<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<value>|<component>
//
// The timestamp is expected to be in the format: `YYYYMMDD HH:MM:SS.mmm`
//
// No input validation is performed.
func NewEntry(inputLine string) Entry {
	// first, split the line into its components
	parts := strings.Split(inputLine, "|")

	timestamp, _ := time.Parse("20060102 15:04:05.999", parts[0])
	satelliteID, _ := strconv.Atoi(parts[1])
	redHighLimit, _ := strconv.ParseFloat(parts[2], 64)
	yellowHighLimit, _ := strconv.ParseFloat(parts[3], 64)
	yellowLowLimit, _ := strconv.ParseFloat(parts[4], 64)
	redLowLimit, _ := strconv.ParseFloat(parts[5], 64)
	val, _ := strconv.ParseFloat(parts[6], 64)
	component, _ := components[parts[7]]

	return Entry{
		Timestamp:       timestamp,
		SatelliteID:     int32(satelliteID),
		RedHighLimit:    redHighLimit,
		RedLowLimit:     redLowLimit,
		YellowHighLimit: yellowHighLimit,
		YellowLowLimit:  yellowLowLimit,
		Val:             val,
		Component:       component,
	}
}

// Severity represents the severity of a warning.
type Severity string

const (
	RedHigh    Severity = "RED HIGH"
	RedLow     Severity = "RED LOW"
	YellowHigh Severity = "YELLOW HIGH"
	YellowLow  Severity = "YELLOW LOW"
)

// process processes the entry.
func (e Entry) process() *Warning {
	hasPassedThreshold, severity := e.hasPassedThreshold()

	// if the entry hasn't passed the threshold, ignore it
	if !hasPassedThreshold {
		return nil
	}

	// add the entry to the respective slice
	entriesByComponent[e.Component.Name] = append(entriesByComponent[e.Component.Name], e)

	// and check for warnings
	return e.Component.getWarning(severity)
}

// hasPassedThreshold checks if the entry's value has passed the threshold.
func (e Entry) hasPassedThreshold() (bool, Severity) {
	threshold := e.Component.Threshold

	switch threshold.LimitField {
	case "RedHighLimit":
		return e.Val > e.RedHighLimit, RedHigh
	case "RedLowLimit":
		return e.Val < e.RedLowLimit, RedLow
	case "YellowHighLimit":
		return e.Val > e.YellowHighLimit, YellowHigh
	case "YellowLowLimit":
		return e.Val < e.YellowLowLimit, YellowLow
	default:
		return false, RedHigh // this will be disregarded
	}
}

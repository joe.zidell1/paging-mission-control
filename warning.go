// This file holds information about warnings and logging them.
package main

import (
	"encoding/json"
	"fmt"
	"time"
)

// Warning represents a information about an entry that has passed the threshold.
type Warning struct {
	SatelliteID int32     `json:"satelliteId"`
	Severity    Severity  `json:"severity"`
	Component   string    `json:"component"`
	Timestamp   time.Time `json:"timestamp"`
}

// Log logs any warnings to standard out.
func LogWarnings(warnings []Warning) {
	// by converting it to JSON
	b, err := json.MarshalIndent(warnings, "", "    ")
	if err != nil {
		fmt.Println("error:", err)
	} else {
		fmt.Println(string(b))
	}
}

// This is the main entry point for the application.
// This file is responsible for reading the input file and processing each line.
package main

import (
	"bufio"
	"log"
	"os"
)

var inputFilename = "input.txt"

func main() {
	// Open input file and read line by line
	readLines(inputFilename)
}

// readLines reads the input file line by line and processes each line.
func readLines(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// create a slice of warnings
	warnings := make([]Warning, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		warning := processInputLine(scanner.Text())
		if warning != nil {
			warnings = append(warnings, *warning)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Log any warnings
	LogWarnings(warnings)
}

// processInputLine processes the input line and returns a warning if the entry has passed the threshold or nil.
func processInputLine(line string) *Warning {
	entry := NewEntry(line)

	return entry.process()
}
